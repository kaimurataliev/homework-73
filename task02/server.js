const express = require('express');
const Vigenere = require('caesar-salad').Vigenere;


const port = 7000;
const app = express();

const password = 'something';

app.get('/encode/:string', (req, res) => {
    const encodeString = Vigenere.Cipher(password).crypt(req.params.string);
    res.send(encodeString);
});

app.get('/decode/:string', (req, res) => {
    const decodeString = Vigenere.Decipher(password).crypt(req.params.string);
    res.send(decodeString);
});

app.listen(port, () => {
    console.log('port ' + port + ' is running!');
});
